//
//  AppDelegate.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 09.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
}
