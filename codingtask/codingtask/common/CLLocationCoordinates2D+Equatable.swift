//
//  CLLocationCoordinates2D+Equatable.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import MapKit

extension CLLocationCoordinate2D: Equatable {}

public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}
