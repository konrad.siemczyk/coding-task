//
//  DateFormatter+yyyyMMdd.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

extension DateFormatter {
    static var yyyyMMdd: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "yyyy/MM/dd"
        return df
    }
}
