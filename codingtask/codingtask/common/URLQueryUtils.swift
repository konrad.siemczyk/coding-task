//
//  URLQueryUtils.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct URLQueryUtils {
    static func appendQuery<T: Encodable>(to url: URL, with encodable: T) throws -> URL {
        let parameters = try encodable.asDictionary()
        
        let queryItems = parameters.map { URLQueryItem(name: $0.key, value: "\($0.value)") }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        urlComponents?.queryItems = queryItems
        
        return urlComponents?.url ?? url
    }
}
