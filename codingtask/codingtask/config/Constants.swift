//
//  Constants.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct Constants {
    struct App {
        static let baseUrl = "http://musicbrainz.org"
    }
    
    struct Config {
        static let beginYear = 1990
        static let limitQuery = 20
    }
}
