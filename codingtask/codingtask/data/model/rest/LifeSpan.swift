//
//  LifeSpan.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct LifeSpan: Decodable {
    var begin: String
}
