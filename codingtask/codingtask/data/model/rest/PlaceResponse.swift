//
//  PlaceResponse.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct PlaceResponse: Decodable {
    var name: String
    var coordinates: CoordinatesResponse?
    var lifeSpan: LifeSpan
    
    private enum CodingKeys: String, CodingKey {
        case name
        case coordinates
        case lifeSpan = "life-span"
    }
}
