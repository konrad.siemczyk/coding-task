//
//  PlacesResponse.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct PlacesResponse: Decodable {
    var places: [PlaceResponse]
    var count: Int
    var offset: Int
}
