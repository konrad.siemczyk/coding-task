//
//  PlacesSearchRequest.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct PlacesSearchRequest: Encodable {
    var query: String
    var format: ResponseFormat
    var limit: Int
    var offset: Int
    
    private enum CodingKeys: String, CodingKey {
        case query
        case format = "fmt"
        case limit
        case offset
    }
}
