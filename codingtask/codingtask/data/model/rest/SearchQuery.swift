//
//  SearchQuery.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct SearchQuery: Encodable {
    var ended: Bool
    var beginDate: Date
    var place: String
}

extension SearchQuery {
    func toLuceneFormat() -> String {
        let dateFormatter = DateFormatter.yyyyMMdd
        let beginDateString = dateFormatter.string(from: beginDate)
        return "\(place) AND begin:{\(beginDateString) TO *} AND ended:\(ended)"
    }
}
