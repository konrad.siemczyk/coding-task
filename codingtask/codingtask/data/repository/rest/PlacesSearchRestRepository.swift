//
//  PlacesSearchRestRepository.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

protocol PlacesSearchRestRepositoryType {
    func search(with request: PlacesSearchRequest, completion: @escaping (PlacesResponse?) -> Void)
}

final class PlacesSearchRestRepository: PlacesSearchRestRepositoryType {
    private let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func search(with request: PlacesSearchRequest, completion: @escaping (PlacesResponse?) -> Void) {
        let baseUrl = URL(string: Constants.App.baseUrl)
        guard let url = URL(string: "/ws/2/place", relativeTo: baseUrl) else { return }
        guard let urlWithParams = try? URLQueryUtils.appendQuery(to: url, with: request) else {
            fatalError("Wrong search parameters!")
        }
        
        session.dataTask(with: urlWithParams, completionHandler: { data, response, error in
            guard let data = data else { return }
            let decoder = JSONDecoder()
            let placesResponse = try? decoder.decode(PlacesResponse.self, from: data)
            completion(placesResponse)
        }).resume()
    }
}
