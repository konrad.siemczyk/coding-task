//
//  Coordinates.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct Coordinates {
    var latitude: Double
    var longitude: Double
    
    init?(latitude: String, longitude: String) {
        guard let latitude = Double(latitude),
            let longitude = Double(longitude) else {
                return nil
        }
        self.init(latitude: latitude, longitude: longitude)
    }
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
