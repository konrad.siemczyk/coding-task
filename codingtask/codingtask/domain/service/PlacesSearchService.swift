//
//  PlacesSearchService.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 10.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

protocol PlacesSearchServiceType {
    func search(with place: String, completion: @escaping ([Place]) -> Void)
    func search(with place: String, queryLimit: Int, completion: @escaping ([Place]) -> Void)
}

final class PlacesSearchService: PlacesSearchServiceType {
    private let restRepository: PlacesSearchRestRepositoryType
    
    init(restRepository: PlacesSearchRestRepositoryType = PlacesSearchRestRepository()) {
        self.restRepository = restRepository
    }
    
    func search(with place: String, completion: @escaping ([Place]) -> Void) {
        search(with: place, queryLimit: Constants.Config.limitQuery, completion: completion)
    }
    
    func search(with place: String, queryLimit: Int, completion: @escaping ([Place]) -> Void) {
        let request = createSearchRequest(for: place, with: queryLimit)
        paginatedSearch(with: request, completion: completion)
    }
    
    private func paginatedSearch(with request: PlacesSearchRequest, completion: @escaping ([Place]) -> Void) {
        restRepository.search(with: request, completion: { [weak self] response in
            guard let response = response, let `self` = self else {
                return
            }
            let places = self.convertPlaces(from: response.places)
            completion(places)
            let newOffset = response.offset + request.limit
            if newOffset < response.count {
                var req = request
                req.offset = newOffset
                self.paginatedSearch(with: req, completion: completion)
            }
        })
    }
    
    private func createSearchRequest(for place: String, with queryLimit: Int) -> PlacesSearchRequest {
        let df = DateFormatter.yyyyMMdd
        let dateString = "\(Constants.Config.beginYear)/01/01"
        let beginDate = df.date(from: dateString) ?? Date()
        let query = SearchQuery(ended: false, beginDate: beginDate, place: place)
        return PlacesSearchRequest(query: query.toLuceneFormat(),
                                   format: .json,
                                   limit: queryLimit,
                                   offset: 0)
    }
    
    private func convertPlaces(from responsePlaces: [PlaceResponse]) -> [Place] {
        return responsePlaces.compactMap { respPlace -> Place? in
            guard let respCoordinates = respPlace.coordinates,
                let coordinates = Coordinates(latitude: respCoordinates.latitude, longitude: respCoordinates.longitude) else {
                    return nil
            }
            
            return Place(name: respPlace.name, coordinates: coordinates, lifeSpan: respPlace.lifeSpan)
        }
    }
}
