//
//  PlacesViewController.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 09.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import UIKit
import MapKit

final class PlacesViewController: UIViewController {
    fileprivate let viewModel = PlacesViewModel()
    
    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.placesDelegate = self
        mapView.delegate = self
        searchBar.delegate = self
    }
    
    fileprivate func addPinToMap(for coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        DispatchQueue.main.async {
            self.mapView.addAnnotation(annotation)
        }
    }
    
    fileprivate func removePinFromMap(for coordinate: CLLocationCoordinate2D) {
        DispatchQueue.main.async {
            guard let annotationToRemove = self.mapView.annotations.first(where: { $0.coordinate == coordinate }) else {
                return
            }
            self.mapView.removeAnnotation(annotationToRemove)
        }
    }
    
    fileprivate func removeAllPins() {
        DispatchQueue.main.async {
            let allAnnotations = self.mapView.annotations
            self.mapView.removeAnnotations(allAnnotations)
        }
    }
    
    fileprivate func setRegion(for searchTerm: String?) {
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchTerm
        let search = MKLocalSearch(request: request)
        search.start(completionHandler: { [weak self] resp ,_ in
            if let region = resp?.boundingRegion {
                self?.mapView.setRegion(region, animated: true)
            }
        })
    }
}

extension PlacesViewController: PlacesDelegate {
    func onPlacesAdded(_ newPlaces: [Place]) {
        for place in newPlaces {
            let coordinate = CLLocationCoordinate2D(latitude: place.coordinates.latitude, longitude: place.coordinates.longitude)
            addPinToMap(for: coordinate)
        }
    }
    
    func onPlaceRemoved(_ place: Place) {
        let coordinate = CLLocationCoordinate2D(latitude: place.coordinates.latitude, longitude: place.coordinates.longitude)
        removePinFromMap(for: coordinate)
    }
    
    func onNewSearch() {
        removeAllPins()
    }
}

extension PlacesViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchTerm = searchBar.text
        setRegion(for: searchTerm)
        viewModel.searchForPlaces(for: searchTerm)
        searchBar.endEditing(true)
    }
}

extension PlacesViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MKPointAnnotation else { return nil }
        
        let identifier = MKMapViewDefaultAnnotationViewReuseIdentifier
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
        view.annotation = annotation
        view.displayPriority = .required
        return view
    }
}
