//
//  PlacesViewModel.swift
//  codingtask
//
//  Created by Konrad Siemczyk on 09.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

protocol PlacesDelegate: class {
    func onPlacesAdded(_ newPlaces: [Place])
    func onPlaceRemoved(_ place: Place)
    func onNewSearch()
}

final class PlacesViewModel {
    private let searchService: PlacesSearchServiceType
    private var tasks = [DispatchWorkItem]()
    
    weak var placesDelegate: PlacesDelegate?
    
    init(searchService: PlacesSearchServiceType = PlacesSearchService()) {
        self.searchService = searchService
    }
    
    func searchForPlaces(for searchTerm: String?) {
        guard let searchTerm = searchTerm, !searchTerm.isEmpty else {
            return
        }
        
        cancelCurrentTasks()
        placesDelegate?.onNewSearch()
        searchService.search(with: searchTerm, completion: { [weak self] newPlaces in
            newPlaces.forEach {
                self?.applyLifeSpan(for: $0)
            }
            self?.placesDelegate?.onPlacesAdded(newPlaces)
        })
    }
    
    private func applyLifeSpan(for place: Place) {
        let beginYearString = place.lifeSpan.begin.prefix(4)
        guard let year = Int(beginYearString) else {
            return
        }
        let lifeSpan = TimeInterval(year - Constants.Config.beginYear)
        let task = DispatchWorkItem(block: { [weak self] in
            self?.placesDelegate?.onPlaceRemoved(place)
        })
        DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + lifeSpan, execute: task)
        tasks.append(task)
    }
    
    private func cancelCurrentTasks() {
        tasks.forEach { $0.cancel() }
        tasks = []
    }
}
