//
//  EncodableAsDictionaryTests.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//


import Foundation
import XCTest
@testable import codingtask

final class EncodableAsDictionaryTests: XCTestCase {

    func testDictionaryConversion() {
        let encodable = TestEncodable(param1: "test", param2: 123)
        do {
            let sut = try encodable.asDictionary()
            XCTAssertTrue((sut["param1"] as? String) == "test")
            XCTAssertTrue((sut["param2"] as? Int) == 123)
        } catch {
            XCTFail("conversion failed!")
        }
    }
}
