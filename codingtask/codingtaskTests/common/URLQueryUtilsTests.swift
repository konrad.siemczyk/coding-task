//
//  URLQueryUtilsTests.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
import XCTest
@testable import codingtask

final class URLQueryUtilsTests: XCTestCase {
    
    func testAppendQuery() {
        let encodable = TestEncodable(param1: "123", param2: 321)
        let url = URL(string: "https://google.com")!
        do {
            let sut = try URLQueryUtils.appendQuery(to: url, with: encodable)
            XCTAssertNotEqual(url, sut)
            XCTAssertTrue(sut.absoluteString.contains(encodable.param1))
            XCTAssertTrue(sut.absoluteString.contains("\(encodable.param2)"))
        } catch {
            XCTFail("append query failed!")
        }
    }
}
