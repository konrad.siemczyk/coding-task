//
//  TestEncodable.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation

struct TestEncodable: Encodable {
    var param1: String
    var param2: Int
}
