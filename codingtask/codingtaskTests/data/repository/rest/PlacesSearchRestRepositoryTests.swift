//
//  PlacesSearchRestRepositoryTests.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
import XCTest
@testable import codingtask

final class PlacesSearchRestRepositoryTests: XCTestCase {
    
    private var sessionStub = URLSessionStub()
    
    override func setUp() {
        super.setUp()
        
        createMockData()
    }
    
    private func createMockData() {
        let jsonData = """
        {"created":"2018-06-11T09:36:02.513Z","count":45,"offset":5,"places":[{"id":"fe35974a-8132-4a28-9884-40dcb63d851a","type":"Indoor arena","score":"52","name":"Budweiser Gardens","address":"99 Dundas St, London, ON N6A 6K1","coordinates":{"latitude":"42.9825","longitude":"-81.2525"},"area":{"id":"db3634e7-5414-41dd-be0b-68ae71798dcd","name":"London","sort-name":"London"},"life-span":{"begin":"2002-10-11","ended":null},"aliases":[{"sort-name":"John Labatt Centre","name":"John Labatt Centre","locale":"en","type":"Place name","primary":null,"begin-date":"2002","end-date":"2012"}]},{"id":"3e7a47e7-4f33-4a93-8d89-5e9e3866cf37","type":"Studio","score":"52","name":"Alchemy Studio","disambiguation":"London studio owned by Kenny Jones","address":"140 Station Rd, London N22 7SX","coordinates":{"latitude":"51.597103","longitude":"-0.117335"},"area":{"id":"f03d09b3-39dc-4083-afd6-159e3f0d462f","name":"London","sort-name":"London"},"life-span":{"begin":"2001","ended":null}},{"id":"b53d8208-8b5b-4c3b-a98b-43ccc8b003ba","type":"Venue","score":"52","name":"Kings Place","address":"90 York Way, London, N1 9AG","coordinates":{"latitude":"51.534443","longitude":"-0.122264"},"area":{"id":"f03d09b3-39dc-4083-afd6-159e3f0d462f","name":"London","sort-name":"London"},"life-span":{"begin":"2008","ended":null}}]}
        """.data(using: .utf8)
        sessionStub.data = jsonData
    }
    
    func testSearch() {
        let sut = PlacesSearchRestRepository(session: sessionStub)
        let beginDate = DateFormatter.yyyyMMdd.date(from: "1995/01/01")!
        let query = SearchQuery(ended: false, beginDate: beginDate, place: "London")
        let request = PlacesSearchRequest(query: query.toLuceneFormat(), format: .json, limit: 50, offset: 0)
        
        let expectation = XCTestExpectation(description: "Expect search results from an API")
        
        sut.search(with: request, completion: { response in
            XCTAssertNotNil(response)
            XCTAssertFalse(response?.places.count == 0)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 2)
    }
}
