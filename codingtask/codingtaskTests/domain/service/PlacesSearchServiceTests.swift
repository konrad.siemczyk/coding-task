//
//  PlacesSearchServiceTests.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
import XCTest
@testable import codingtask

final class PlacesSearchServiceTests: XCTestCase {
    
    func testSearch() {
        let sut = PlacesSearchService(restRepository: PlacesSearchRestRepositoryStub())
        let searchTerm = "test"
        
        let expectation = XCTestExpectation(description: "Expect search results from a service")
        expectation.expectedFulfillmentCount = 1
        
        sut.search(with: searchTerm, queryLimit: 5, completion: { places in
            XCTAssertTrue(places.count == 3)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 2)
    }
    
    func testPaginatedSearch() {
        let sut = PlacesSearchService(restRepository: PlacesSearchRestRepositoryStub())
        let searchTerm = "test"
        
        let expectation = XCTestExpectation(description: "Expect search results from a service")
        expectation.expectedFulfillmentCount = 3
        
        sut.search(with: searchTerm, queryLimit: 1, completion: { places in
            XCTAssertTrue(places.count == 1)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
}

