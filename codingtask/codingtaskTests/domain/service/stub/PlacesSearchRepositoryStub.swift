//
//  PlacesSearchRepositoryStub.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
@testable import codingtask

final class PlacesSearchRestRepositoryStub: PlacesSearchRestRepositoryType {
    func search(with request: PlacesSearchRequest, completion: @escaping (PlacesResponse?) -> Void) {
        let place1 = PlaceResponse(name: "test1", coordinates: CoordinatesResponse(latitude: "12.02", longitude: "13.03"), lifeSpan: LifeSpan(begin: "1992"))
        let place2 = PlaceResponse(name: "test1", coordinates: CoordinatesResponse(latitude: "55.02", longitude: "33.03"), lifeSpan: LifeSpan(begin: "\(Constants.Config.beginYear - 1)"))
        let place3 = PlaceResponse(name: "test1", coordinates: CoordinatesResponse(latitude: "6.02", longitude: "16.33"), lifeSpan: LifeSpan(begin: "2002"))
        let places = [place1, place2, place3]
        let placesToReturn = Array(places.suffix(from: request.offset).prefix(request.limit))
        let response = PlacesResponse(places: placesToReturn, count: places.count, offset: request.offset)
        completion(response)
    }
}
