//
//  PlacesViewModelTests.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
import XCTest
@testable import codingtask

final class PlacesViewModelTests: XCTestCase {
    
    func testSearchForPlaces() {
        let sut = PlacesViewModel(searchService: PlacesSearchServiceStub())
        let delegate = PlacesDelegateStub()
        delegate.newPlacesExpectation = XCTestExpectation(description: "Expect for new places")
        delegate.placeRemovedExpectation = XCTestExpectation(description: "Expect for place to be removed")
        delegate.newSearchExpectation = XCTestExpectation(description: "Expect for new search")
        sut.placesDelegate = delegate
        
        sut.searchForPlaces(for: "test")
        
        wait(for: [delegate.newSearchExpectation!, delegate.newPlacesExpectation!], timeout: 2, enforceOrder: true)
    }
    
    func testSearchResultsLifeSpan() {
        let sut = PlacesViewModel(searchService: PlacesSearchServiceStub())
        let delegate = PlacesDelegateStub()
        delegate.placeRemovedExpectation = XCTestExpectation(description: "Expect for place to be removed")
        delegate.placeRemovedExpectation?.expectedFulfillmentCount = 2
        sut.placesDelegate = delegate
        
        sut.searchForPlaces(for: "test")
        
        wait(for: [delegate.placeRemovedExpectation!], timeout: 15)
    }
    
    func testInvalidSearchTerm() {
        let sut = PlacesViewModel(searchService: PlacesSearchServiceStub())
        let delegate = PlacesDelegateStub()
        delegate.newPlacesExpectation = XCTestExpectation(description: "Expect for new places")
        delegate.placeRemovedExpectation = XCTestExpectation(description: "Expect for place to be removed")
        delegate.newSearchExpectation = XCTestExpectation(description: "Expect for new search")
        sut.placesDelegate = delegate
        
        sut.searchForPlaces(for: nil)
        sut.searchForPlaces(for: "")
        
        let result = XCTWaiter().wait(for: [delegate.newPlacesExpectation!, delegate.newSearchExpectation!, delegate.placeRemovedExpectation!], timeout: 3)
        XCTAssertTrue(result == .timedOut)
    }
}
