//
//  PlacesDelegateStub.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
import XCTest
@testable import codingtask

final class PlacesDelegateStub: PlacesDelegate {
    var newPlacesExpectation: XCTestExpectation?
    var placeRemovedExpectation: XCTestExpectation?
    var newSearchExpectation: XCTestExpectation?

    func onPlacesAdded(_ newPlaces: [Place]) {
        newPlacesExpectation?.fulfill()
    }
    
    func onPlaceRemoved(_ place: Place) {
        placeRemovedExpectation?.fulfill()
    }
    
    func onNewSearch() {
        newSearchExpectation?.fulfill()
    }
}
