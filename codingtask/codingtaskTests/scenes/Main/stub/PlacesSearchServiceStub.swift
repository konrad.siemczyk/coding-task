//
//  PlacesSearchServiceStub.swift
//  codingtaskTests
//
//  Created by Konrad Siemczyk on 11.06.2018.
//  Copyright © 2018 Konrad Siemczyk. All rights reserved.
//

import Foundation
@testable import codingtask

final class PlacesSearchServiceStub: PlacesSearchServiceType {
    func search(with place: String, completion: @escaping ([Place]) -> Void) {
        search(with: place, queryLimit: Constants.Config.limitQuery, completion: completion)
    }
    
    func search(with place: String, queryLimit: Int, completion: @escaping ([Place]) -> Void) {
        let place1 = Place(name: "test1", coordinates: Coordinates(latitude: 2, longitude: 3), lifeSpan: LifeSpan(begin: "1995"))
        let place2 = Place(name: "test2", coordinates: Coordinates(latitude: 3, longitude: 2), lifeSpan: LifeSpan(begin: "1991"))
        let places = [place1, place2]
        completion(places)
    }
}
